#pragma once

#define SMALL_STRING_CAPACITY 256

struct SmallString {
  char s[SMALL_STRING_CAPACITY];
  int len;
};

void clearSmallString(SmallString *str) {
  memset(str->s, '\0', sizeof(str->s));
  str->len = 0;
}

void append(SmallString *str, char c) {
  if ( str->len >= (SMALL_STRING_CAPACITY - 1) ) {
    str->s[SMALL_STRING_CAPACITY - 2] = '~';
  } else {
    str->s[str->len++] = c;
  }
}
