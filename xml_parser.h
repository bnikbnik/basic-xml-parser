#pragma once

#include "small_string.h"
#include "tag_stack.h"

enum ParseMode {
  TEXT = 0,
  COMMENT,
  CDATA,
  OPEN_TAG_SCAN,
  ATTRIBUTE_READ,
  ATTRIBUTE_VALUE_READ,
  CLOSING_TAG_SCAN,

  TOTAL_MODES
};

const char* ParseModeStr[TOTAL_MODES] = {
  "TEXT",
  "COMMENT",
  "CDATA",
  "OPEN_TAG_SCAN",
  "ATTRIBUTE_READ",
  "ATTRIBUTE_VALUE_READ",
  "CLOSING_TAG_SCAN",
};

struct XmlParser {
  ParseMode mode;
  char* xml;
  size_t xml_size;
  unsigned int cursor;
  int looking_at_prolog_arguments;
  int visited_at_least_one_tag;
};

char peekAt(XmlParser* parser, int offset) {
  unsigned int index = parser->cursor + offset;
  if ( index >= parser->xml_size ) { return 0; }
  return parser->xml[index];
}

char peek(XmlParser* parser) {
  return peekAt(parser, 0);
}

int sequenceFollows(XmlParser* parser, const char* str) {
  int str_size = strlen(str);

  if ( parser->cursor + str_size > parser->xml_size ) {
    return false;
  }

  for ( int it = 0; it < str_size; it++ ) {
    if ( peekAt(parser, it) != str[it] ) {
      return false;
    }
  }

  return true;
}

char consume(XmlParser* parser) {
  char c = peek(parser);
  parser->cursor++;
  return c;
}

void skipAhead(XmlParser* parser, unsigned int offset) {
  parser->cursor = parser->cursor += offset;
  if ( parser->cursor >= parser->xml_size ) {
    parser->cursor = parser->xml_size - 1;
  }
}

void printContext(XmlParser* parser) {
  printf("\nParsing Context: ");
  for( int offset = -20; offset < 20; offset++ ) {
    char c = peekAt(parser, offset);
    if ( !c ) { continue; }

    if ( c == '\n' || c == '\r' ) { c = ' '; }
    if ( offset != -1 ) {
      printf("%c", c);
    } else {
      printf("\u001b[31m%c\u001b[0m", c);
    }
  }
  printf("\n");
}

int isWhitespace(char c) {
  return (c == ' ' || c == '\t' || c == '\r' || c == '\n');
}

int isAlpha(char c) {
  return (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z');
}

int isNumber(char c) {
  return (c >= '0' && c <= '9');
}

int isValidTagFirstChar(char c) {
  return isAlpha(c) || ( c == '_' );
}

int isValidTagChar(char c) {
  return isAlpha(c) || isNumber(c) ||  (c == '-') || ( c == '_' ) || ( c == '.' );
}

int isValidAttrName(char c) {
  return isAlpha(c) || (c == '-') || (c == ':') || (c == '_');
}

void parse_xml(char* xml) {
  XmlParser parser = {
    .mode = TEXT,
    .xml = xml,
    .xml_size = strlen(xml),
    .cursor = 0,
    .looking_at_prolog_arguments = 0,
    .visited_at_least_one_tag = 0
  };

  TagStack active_tag_path = initTagStack();
  SmallString current_tag;
  SmallString current_attr;
  SmallString current_value;

  int max_tag_stack_count = 0;

  while (parser.cursor < parser.xml_size) {
    char current_char = consume(&parser);

    if ( active_tag_path.count > max_tag_stack_count) { max_tag_stack_count = active_tag_path.count; }
    // printf("C:%d/%zu, v:%c, mode:%s\n", parser.cursor, parser.xml_size, current_char, ParseModeStr[parser.mode]);


    if ( parser.mode == TEXT ) {
      if (current_char == '<') {
        char next_char = peek(&parser);

        if ( next_char == '/') {
          skipAhead(&parser, 1);
          clearSmallString(&current_tag);

          parser.mode = CLOSING_TAG_SCAN;
          continue;
        } else if ( isValidTagFirstChar(next_char) ) {
          parser.visited_at_least_one_tag = 1;
          clearSmallString(&current_tag);

          parser.mode = OPEN_TAG_SCAN;
          continue;
        } else if ( next_char == '!' ) {
          skipAhead(&parser, 1);

          if ( sequenceFollows(&parser, "--" ) ) {
            skipAhead(&parser, 2);

            parser.mode = COMMENT;
            continue;
          } else if ( sequenceFollows(&parser, "[CDATA[") ) {
            skipAhead(&parser, 7);

            parser.mode = CDATA;
            continue;
          } else {
            printf("\nParse error. Special tag (comment or cdata?) doesn't seem to be well-defined.\n");
            printContext(&parser);
            break;
          }
        } else if ( next_char == '?' && !parser.visited_at_least_one_tag ) {
          skipAhead(&parser, 1);

          if ( sequenceFollows(&parser, "xml") ) {
            skipAhead(&parser, 3);
            parser.looking_at_prolog_arguments = 1;
            clearSmallString(&current_attr);

            printf("> Entering: Prolog\n");
            parser.mode = ATTRIBUTE_READ;
            continue;
          } else {
            printf("\nParse error. Prolog must start with <?xml\n");
            printContext(&parser);
            break;
          }
        } else {
          printf("\nParse error. Tag name can only begin with a letter. This one started with %c\n", next_char);
          printContext(&parser);
          break;
        }
      }

      continue;
    }

    if ( parser.mode == COMMENT ) {
      if ( current_char == '-' &&  sequenceFollows(&parser, "->") ) {
        skipAhead(&parser, 2);

        parser.mode = TEXT;
        continue;
      }

      continue;
    }

    if ( parser.mode == CDATA ) {
      if ( current_char == ']' && sequenceFollows(&parser, "]>") ) {
        skipAhead(&parser, 2);

        parser.mode = TEXT;
        continue;
      }

      continue;
    }

    if ( parser.mode == OPEN_TAG_SCAN ) {
      if ( isWhitespace(current_char) ) {
        // @Note Even if the tag is in the form <p > it's fine to get into the
        // attribute read phase since it will end correctly with no attributes
        // read anyway.
        printf("> Entering: %s\n", current_tag.s);
        addTagToStack(&active_tag_path, &current_tag);
        clearSmallString(&current_attr);

        parser.mode = ATTRIBUTE_READ;
        continue;
      }

      if ( current_char == '>' ) {
        printf("> Entering: %s\n", current_tag.s);
        addTagToStack(&active_tag_path, &current_tag);

        parser.mode = TEXT;
        continue;
      }

      if ( isValidTagChar(current_char) ) {
        append(&current_tag, current_char);

        continue;
      } else {
        printf("\nParse error. Invalid character %c for tag. C:%d\n", current_char, parser.cursor);
        printContext(&parser);
        break;
      }
    }

    if ( parser.mode == CLOSING_TAG_SCAN ) {
      if ( current_char == '>' ) {
        if ( active_tag_path.count > 0 ) {
          SmallString* current_active_tag = peekTagFromStack(&active_tag_path);
          if ( strcmp(current_active_tag->s, current_tag.s) == 0 ) {
            printf("< Exiting: %s\n", current_active_tag->s);
            popTagFromStack(&active_tag_path);

            parser.mode = TEXT;
            continue;
          } else {
            printf("\nParse error. Closing tag found %s, but expecting %s", current_tag.s, current_active_tag->s);
            printContext(&parser);
            break;
          }
        } else {
          printf("\nParse error. Closing tag found %s, but no tag is open.", current_tag.s);
          printContext(&parser);
          break;
        }
      }

      if ( isWhitespace(current_char) ) {
        continue;
      }

      if ( isValidTagChar(current_char) ) {
        append(&current_tag, current_char);

        continue;
      } else {
        printf("\nParse error. Invalid character %c for tag. C:%d\n", current_char, parser.cursor);
        printContext(&parser);
        break;
      }
    }

    if ( parser.mode == ATTRIBUTE_READ ) {
      if ( parser.looking_at_prolog_arguments ) {
        if ( current_char == '?' ) {
          if ( peek(&parser) == '>' ) {
            printf("< Exiting: Prolog\n");
            parser.looking_at_prolog_arguments = 0;

            parser.mode = TEXT;
            continue;
          } else {
            printf("\nParse error. ? not followed by > found in prolog.");
            printContext(&parser);
            break;
          }
        }
      } else {
        if ( current_char == '/' ) {
          // @Note We don't do anything we'll check the next character next
          // round.
          continue;
        }

        if ( current_char == '>' ) {
          if ( peekAt(&parser, -2) == '/' ) {
            // @Note It's a /> so the tag is out of the active path
            printf("< Exiting: %s\n", peekTagFromStack(&active_tag_path)->s);
            popTagFromStack(&active_tag_path);
          }

          parser.mode = TEXT;
          continue;
        }
      }

      if ( isWhitespace(current_char) ) {
        continue;
      }

      if ( isValidAttrName(current_char) ) {
        append(&current_attr, current_char);

        continue;
      }

      if ( current_char == '=' ) {
        if ( peek(&parser) == '"' ) {
          skipAhead(&parser, 1);

          printf("  - %s", current_attr.s);
          clearSmallString(&current_value);

          parser.mode = ATTRIBUTE_VALUE_READ;
          continue;
        } else {
          printf("\nParse error. Attribute value for %s MUST be enclosed in quotes.", current_attr.s);
          printContext(&parser);
          break;
        }
      }
    }

    if ( parser.mode == ATTRIBUTE_VALUE_READ ) {
      if ( current_char == '"' && peekAt(&parser, -2) != '\\' ) {
        printf(" -> %s\n", current_value.s);

        clearSmallString(&current_attr);
        parser.mode = ATTRIBUTE_READ;
        continue;
      }

      append(&current_value, current_char);
      continue;
    }

    // @Note If we get here we read something we didn't understand.
    printf("\nUnhandled character %c in mode %s", current_char, ParseModeStr[parser.mode]);
    printContext(&parser);
    break;
  }

  printf("\nActive Tag Pax max count was %d/%d", max_tag_stack_count, active_tag_path.capacity);
  free(active_tag_path.tags);
}
