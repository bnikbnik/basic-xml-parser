#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xml_parser.h"

//@Todo a structure to query the xml after parsing.
// struct Node {
//   int start;
//   int end;
//   HashTable* attributes;
//   Nodes* content_nodes;
// }

char* readEntireFile(char* path) {
  FILE* f;

  if ( path && fopen_s(&f, path, "rb") == 0 ) {
    fseek(f, 0, SEEK_END);
    size_t file_size = ftell(f) + 1;
    fseek(f, 0, SEEK_SET);

    char* buffer = (char *)malloc(file_size * sizeof(char) );
    fread(buffer, 1, file_size, f);
    buffer[file_size-1] = '\0';
    fclose(f);

    return buffer;
  } else {
    return 0;
  }
}

int main(int argc, char** argv) {
  printf("XML Parser v0.9\n\n");

  if ( argc < 2 ) {
    printf("Please pass the path to the xml file you want to parse as a parameter.");
    return 1;
  }

  char* xml_file = argv[1];
  char* xml = readEntireFile(xml_file);

  if ( xml ) {
    parse_xml(xml);
  } else {
    printf("Cannot read file %s. Make sure it exists and try again.", xml_file);
  }

  return 0;
}
