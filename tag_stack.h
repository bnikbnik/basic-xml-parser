#pragma once

#include "small_string.h"

struct TagStack {
  SmallString* tags;
  int capacity;
  int count;
};

TagStack initTagStack(int initial_capacity = 10) {
  int memory_size = initial_capacity*sizeof(SmallString);

  TagStack tag_stack = {
    .tags = (SmallString*)malloc(memory_size),
    .capacity = initial_capacity,
    .count = 0,
  };

  return tag_stack;
}

void addTagToStack(TagStack* tag_stack, SmallString* tag) {
  if ( tag_stack->count >= tag_stack->capacity ) {
    tag_stack->capacity *= 2;

    int memory_size = tag_stack->capacity*sizeof(SmallString);
    tag_stack->tags = (SmallString*)realloc(tag_stack->tags, memory_size);
  }

  tag_stack->tags[tag_stack->count] = *tag;
  tag_stack->count++;
}

SmallString* popTagFromStack(TagStack* tag_stack) {
  if ( tag_stack->count > 0 ) {
    return &tag_stack->tags[--tag_stack->count];
  } else {
    return 0;
  }
}

SmallString* peekTagFromStack(TagStack* tag_stack) {
  return &tag_stack->tags[tag_stack->count-1];
}
